<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport"
     content="width=device-width, initial-scale=1, user-scalable=yes">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">


</head>
<body>
    <div class="container">

        <?php

        $validation = \Config\Services::validation();


        ?>
        <h2 class="text-center mt-4 mb-4">Edit User CRUD System</h2>

        <div class="card">
            <div class="card-header">Edit User Data</div>
            <div class="card-body">
                <form method="post" action="<?php echo base_url('Crud/edit_validation'); ?>">
                  <div class="form-group">
                      <label>First Name</label>
                      <input type="text" name="FIRST_NAME" class="form-control"value="<?php echo $user_data['FIRST_NAME'];?> "/>
                      <?php
                      if($validation->getError('FIRST_NAME'))
                      {
                          echo '<div class="alert alert-danger mt-2">'.$validation->getError('FIRST_NAME').'</div>';
                      }
                      ?>
                  </div>
                  <div class="form-group">
                      <label>Last Name</label>
                      <input type="text" name="LAST_NAME" class="form-control"value="<?php echo $user_data['LAST_NAME'];?> " />
                      <?php
                      if($validation->getError('LAST_NAME'))
                      {
                          echo '<div class="alert alert-danger mt-2">'.$validation->getError('LAST_NAME').'</div>';
                      }
                      ?>
                  </div>

                  <div class="form-group">
                      <label>Email</label>
                      <input type="text" name="EMAIL" class="form-control"value="<?php echo $user_data['EMAIL'];?> " />
                      <?php
                      if($validation->getError('EMAIL'))
                      {

                          echo '<div class="alert alert-danger mt-2">'.$validation->getError('EMAIL').'</div>';
                      }
                      ?>
                  </div>

                    <div class="form-group">
                        <label>Gender</label>
                        <select name="GENDER" class="form-control">
                            <option value="">Select Gender</option>
                            <option value="Male" <?php if($user_data['GENDER'] == 'Male') echo 'selected'; ?>>Male</option>
                            <option value="Female" <?php if($user_data['GENDER'] == 'Female') echo 'selected'; ?>>Female</option>
                        </select>

                        <?php

                        if($validation->getError('GENDER'))
                        {
                            echo '<div class="alert alert-danger mt-2">'.$validation->getError('GENDER').'</div>';
                        }

                        ?>
                    </div>
                    <div class="form-group">
                        <label>Birthday</label>
                        <input type="text" name="BIRTHDAY" class="form-control"value="<?php echo $user_data['BIRTHDAY'];?> " />
                        <?php
                        if($validation->getError('BIRTHDAY'))
                        {
                            echo '<div class="alert alert-danger mt-2">'.$validation->getError('BIRTHDAY').'</div>';
                        }
                        ?>
                    </div>

                    <div class="form-group">
                        <input type="hidden" name="ID" value="<?php echo $user_data["ID"]; ?>" />
                        <input type="submit" class="btn btn-primary" value = "Edit"button style="background-color:#800080;border-color:#800080;color:white;">
                        <a href="http://localhost:8080/Crud">
                        <input type="button"class ="btn btn-primary"value="Back"/>
                        </a>
                    </div>
                </form>
            </div>
        </div>

    </div>

</body>
</html>
