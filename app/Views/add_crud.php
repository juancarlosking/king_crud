<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport"
     content="width=device-width, initial-scale=1, user-scalable=yes">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">


</head>
<body>
    <div class="container">

        <h2 class="text-center mt-4 mb-4">CRUD System</h2>

        <?php

        $validation = \Config\Services::validation();

        ?>
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col">Data Entry</div>
                    <div class="col text-right">

                    </div>
                </div>
            </div>
            <div class="card-body">
                <form method="post" action="<?php echo base_url("Crud/add_validation")?>">
                    <div class="form-group">
                        <label>First Name</label>
                        <input type="text" name="FIRST_NAME" class="form-control" />
                        <?php
                        if($validation->getError('FIRST_NAME'))
                        {
                            echo '<div class="alert alert-danger mt-2">'.$validation->getError('FIRST_NAME').'</div>';
                        }
                        ?>
                    </div>

                    <div class="form-group">
                        <label>Last Name</label>
                        <input type="text" name="LAST_NAME" class="form-control" />
                        <?php
                        if($validation->getError('LAST_NAME'))
                        {
                            echo '<div class="alert alert-danger mt-2">'.$validation->getError('LAST_NAME').'</div>';
                        }
                        ?>
                    </div>

                    <div class="form-group">
                        <label>Email</label>
                        <input type="text" name="EMAIL" class="form-control" />
                        <?php
                        if($validation->getError('EMAIL'))
                        {

                            echo '<div class="alert alert-danger mt-2">'.$validation->getError('EMAIL').'</div>';
                        }
                        ?>
                    </div>

                    <div class="form-group">
                        <label>Gender</label>
                        <select name="GENDER" class="form-control">
                            <option value="">Select Gender</option>
                            <option value="Male">Male</option>
                            <option value="Female">Female</option>

                        </select>

                        <?php

                        if($validation->getError('GENDER'))
                        {
                            echo '<div class="alert alert-danger mt-2">'.$validation->getError('GENDER').'</div>';
                        }

                        ?>
                    </div>
                    <div class="form-group">
                        <label>Birthday</label>
                        <input type="date" name="BIRTHDAY" class="form-control" />
                        <?php
                        if($validation->getError('BIRTHDAY'))
                        {
                            echo '<div class="alert alert-danger mt-2">'.$validation->getError('BIRTHDAY').'</div>';
                        }
                        ?>
                    </div>
                    <div class="form-group">

                        <button type="submit" action="<?php echo base_url("Crud")?>" class="btn btn-primary"button style="background-color:#9370DB;border-color:#9370DB;">Add</button>
                        <a href="http://localhost:8080/Crud">
                        <input type="button"class ="btn btn-primary"value="Back"/>
                        </a>

                    </div>
                </form>
            </div>
        </div>

    </div>

</body>
</html>
