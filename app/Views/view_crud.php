<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport"
     content="width=device-width, initial-scale=1, user-scalable=yes">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">

    <!--  -->
</head>
<body>
    <div class="container">

        <h2 class="text-center mt-4 mb-4">CRUD System</h2>

        <?php

        $session = \Config\Services::session();

        if($session->getFlashdata('success'))
        {
            echo '
            <div class="alert alert-success">'.$session->getFlashdata("success").'</div>
            ';
        }

        ?>
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col">User Data</div>
                    <div class="col text-right">
                    <a href="<?php echo base_url("Crud/add")?>" class="btn btn-success btn-sm" button style="background-color:	#9370DB;border-color:#9370DB;">Add User</a>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered">
                        <tr>
                            <th>ID</th>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Email</th>
                            <th>Gender</th>
                            <th>Birthday</th>
                            <th>Edit</th>
                            <th>Delete</th>
                        </tr>

                        <?php

                        if($user_data)
                        {
                            foreach($user_data as $user)
                            {
                                echo '
                                <tr>
                                    <td>'.$user["ID"].'</td>
                                    <td>'.$user["FIRST_NAME"].'</td>
                                    <td>'.$user["LAST_NAME"].'</td>
                                    <td>'.$user["EMAIL"].'</td>
                                    <td>'.$user["GENDER"].'</td>
                                    <td>'.$user["BIRTHDAY"].'</td>
                                     <td><a href="'.base_url().'/Crud/fetch_single_data/'.$user["ID"].'" class="btn btn-sm btn-warning"button style="background-color:#800080;border-color:#800080;color:white;">Edit</a></td>
                                    <td><button type="button" onclick="delete_data('.$user["ID"].')" class="btn btn-danger btn-sm"button style="background-color:#db7093;border-color:#db7093;color:white">Delete</button></td>
                                </tr>';
                            }
                        }

                        ?>
                    </table>
                </div>
                <div>
                    <?php

                    if($pagination_link)
                    {
                        $pagination_link->setPath('Crud');

                        echo $pagination_link->links();
                    }

                    ?>
                    <style>

                    .pagination li a
                    {
                        position: relative;
                        display: block;
                        padding: .5rem .75rem;
                        margin-left: -1px;
                        line-height: 1.25;
                        color: #007bff;
                        background-color: #FFF;
                        border: 1px solid #dee2e6;
                    }

                    .pagination li.active a {
                        z-index: 1;
                        color: #fff;
                        background-color: #337ab7;
                        border-color: #2e6da4;
                    }
                    </style>

                </div>
            </div>
        </div>

    </div>

</body>
</html>
<script>
function delete_data(id)
{
    if(confirm("Are you sure you want to remove this user?"))
    {
        window.location.href="<?php echo base_url(); ?>Crud/delete/"+id;
    }
    return false;
}
</script>
