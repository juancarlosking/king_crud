<?php
namespace App\Models;

use CodeIgniter\Model;

class CrudModel extends Model
{
protected $table = 'user_table';

protected $primarykey = 'id';

protected $allowedFields =['FIRST_NAME','LAST_NAME','EMAIL','GENDER','BIRTHDAY'];
}

 ?>
