<?php
namespace App\Controllers;

use App\Models\CrudModel;

class Crud extends BaseController
{
  function index()
  {
    // echo 'hello';
    $crudModel = new CrudModel();

    $data['user_data'] = $crudModel->orderBy('ID','ASC')->paginate(10);

    $data['pagination_link'] = $crudModel->pager;

    return view('view_crud', $data);

  }
  function add()
	{
		return view('add_crud');

	}

	function add_validation()
	{
		helper(['form', 'url']);

		$error = $this->validate([
			'FIRST_NAME'	=>	'required|min_length[3]',
      'LAST_NAME'	=>	'required|min_length[3]',
			'EMAIL'	=>	'required|valid_email',
			'GENDER'=>	'required',
      'BIRTHDAY'=>	'required'
		]);

		if(!$error)
		{
			echo view('add_crud', [
				'error' 	=> $this->validator
			]);
		}
		else
		{
			$crudModel = new CrudModel();

			$crudModel->save([
				'FIRST_NAME'	=>	$this->request->getVar('FIRST_NAME'),
        'LAST_NAME'	=>	$this->request->getVar('LAST_NAME'),
				'EMAIL'	=>	$this->request->getVar('EMAIL'),
				'GENDER'=>	$this->request->getVar('GENDER'),
        'BIRTHDAY'=>	$this->request->getVar('BIRTHDAY')
			]);

			$session = \Config\Services::session();

			$session->setFlashdata('success', 'User Data Added');

			return $this->response->redirect(site_url('Crud'));
		}
	}
  function fetch_single_data($id = null)
    {
        $crudModel = new CrudModel();

        $data['user_data'] = $crudModel->where('ID', $id)->first();

        return view('edit_crud', $data);
    }
    function edit_validation()
    {
    	helper(['form', 'url']);

        $error = $this->validate([
          'FIRST_NAME'	=>	'required',
          'LAST_NAME'	=>	'required',
    			'EMAIL'	=>	'required',
    			'GENDER'=>	'required',
          'BIRTHDAY'=>	'required'
        ]);

        $crudModel = new CrudModel();

        $id = $this->request->getVar('ID');

        if(!$error)
        {
        	$data['user_data'] = $crudModel->where('ID', $id)->first();
        	$data['error'] = $this->validator;
        	echo view('edit_crud', $data);
        }
        else
        {
	        $data = [
            'FIRST_NAME'	=>	$this->request->getVar('FIRST_NAME'),
            'LAST_NAME'	=>	$this->request->getVar('LAST_NAME'),
    				'EMAIL'	=>	$this->request->getVar('EMAIL'),
    				'GENDER'=>	$this->request->getVar('GENDER'),
            'BIRTHDAY'=>	$this->request->getVar('BIRTHDAY')
	        ];

        	$crudModel->update($id, $data);

        	$session = \Config\Services::session();

            $session->setFlashdata('success', 'User Updated');

        	return $this->response->redirect(site_url('Crud'));
        }
      }
      function delete($id)
    {
        $crudModel = new CrudModel();

        $crudModel->where('ID', $id)->delete($id);

        $session = \Config\Services::session();

        $session->setFlashdata('success', 'User Deleted');

        return $this->response->redirect(site_url('Crud'));
    }
}

?>
